package ultraemojicombat;

import java.util.Random;

public class Luta
{
    // Atributos
    private Lutador desafiado;
    private Lutador desafiante;
    private int rounds;
    private boolean aprovada;
    
    
    
    // Metodos Publicos
    public void marcarLuta(Lutador l1, Lutador l2) {
        if ( l1 != l2 && l1.getCategoria().equals(l2.getCategoria()) ) {
            this.setAprovada(true);
            this.setDesafiado(l1);
            this.setDesafiante(l2);
        } else {
            this.setAprovada(false);
            this.setDesafiado(null);
            this.setDesafiante(null);
        }
    }
    
    public void lutar() {
        
        if (this.getAprovada()) {
            System.out.println("              ### DESAFIADO ###");
            this.desafiado.apresentar();
            System.out.println("              ### DESAFIANTE ###");
            this.desafiante.apresentar();
        
            System.out.println("-------------------------------------------------");
            
            Random aleatorio = new Random();
            int vencedor = aleatorio.nextInt(3);
            switch (vencedor){
                case 0: // Empate
                    System.out.println("Empatou!");
                    this.desafiado.empatarLuta();
                    this.desafiante.empatarLuta();
                    break;
                case 1: // Desafiado Vence
                    System.out.println("Vitoria do " + this.desafiado.getNome() + "!");
                    this.desafiado.ganharLuta();
                    this.desafiante.perderLuta();
                    break;
                case 2: // Desafiante Vence
                    System.out.println("Vitoria do " + this.desafiante.getNome() + "!");
                    this.desafiado.perderLuta();
                    this.desafiante.ganharLuta();
                    break;
            }
        } else {
            System.out.println("-------------------------------------------------");
            System.out.println("A luta nao pode acontecer!");
        }
        
        System.out.println("-------------------------------------------------");
    }
    
    
    
    // Metodos Epeciais
    public Lutador getDesafiado() {
        return desafiado;
    }

    public void setDesafiado(Lutador desafiado) {
        this.desafiado = desafiado;
    }

    public Lutador getDesafiante() {
        return desafiante;
    }

    public void setDesafiante(Lutador desafiante) {
        this.desafiante = desafiante;
    }

    public int getRounds() {
        return rounds;
    }

    public void setRounds(int rounds) {
        this.rounds = rounds;
    }

    public boolean getAprovada() {
        return aprovada;
    }

    public void setAprovada(boolean aprovada) {
        this.aprovada = aprovada;
    }
}