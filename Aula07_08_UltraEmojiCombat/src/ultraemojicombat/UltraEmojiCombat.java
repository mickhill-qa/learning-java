package ultraemojicombat;

public class UltraEmojiCombat
{
    public static void main(String[] args)
    {
        Lutador l[] = new Lutador[6];
        
        l[0] = new Lutador("Adriano", "USA", 32, (float) 1.68, (float) 57.2, 6, 1, 1);
        l[1] = new Lutador("Tiago", "BRA", 33, (float) 1.67, (float) 68.4, 5, 2, 3);
        l[2] = new Lutador("Helny", "ITA", 34, (float) 1.66, (float) 80.5, 4, 3, 5);
        l[3] = new Lutador("Janderson", "GER", 35, (float) 1.65, (float) 81.8, 3, 4, 2);
        l[4] = new Lutador("Nikael", "AUS", 36, (float) 1.64, (float) 109.9, 2, 5, 4);
        l[5] = new Lutador("Tony", "JAP", 37, (float) 1.63, (float) 105.3, 1, 6, 6);
        
        Luta UEC01 = new Luta();
        UEC01.marcarLuta(l[4], l[5]);
        UEC01.lutar();
        
        l[4].status();
        l[5].status();
    }
}