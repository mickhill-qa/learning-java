package projetoyoutube;

public class ProjetoYouTube
{

    public static void main(String[] args)
    {
       Video videos[] = new Video[4];
       videos[0] = new Video("Aula 01 de POO");
       videos[1] = new Video("Aula 02 de PHP");
       videos[2] = new Video("Aula 03 de HTML5");
       videos[3] = new Video("Aula 04 de CSS3");
       
       Gafanhoto gafanhoto[] = new Gafanhoto[3];
       gafanhoto[0] = new Gafanhoto("Mick Hill",    23, "M", "hill");
       gafanhoto[1] = new Gafanhoto("Priscila",     25, "F", "prihill");
       gafanhoto[2] = new Gafanhoto("Everton Hill",  1, "M", "hilljr");
       
       Visualizacao v[] = new Visualizacao[10];
       v[0] = new Visualizacao(gafanhoto[0], videos[1]); v[0].avaliar();
       v[1] = new Visualizacao(gafanhoto[0], videos[0]); v[0].avaliar(87.0f);
       
        System.out.println("\nVideos\n---------------------------");
        for (Video und: videos) {
            System.out.println(und.toString());
        }
       
        System.out.println("\nGafanhotos\n---------------------------");
        for (Gafanhoto und: gafanhoto) {
            System.out.println(und.toString());
        }
    }
}