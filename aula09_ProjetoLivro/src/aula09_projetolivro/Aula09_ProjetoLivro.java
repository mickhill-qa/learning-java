package aula09_projetolivro;

public class Aula09_ProjetoLivro
{
    public static void main(String[] args)
    {
        Pessoa[] p = new Pessoa[2];
        Livro[]  l = new Livro[3];
        
        p[0] = new Pessoa("Mick Hill", 23, "M");
        p[1] = new Pessoa("Priscila", 25, "F");
        
        l[0] = new Livro("Classroom in a Book", "Adobe inc", 400, p[0]);
        l[1] = new Livro("O Pequeno Principe", "Antoine de Saint-Exupery", 120, p[1]);
        l[2] = new Livro("O Livro de Mormon", "Profetas das Americas", 612, p[1]);
        
        l[0].abrir();
        l[0].folhear(60);
        l[0].avancarPag();
        
        System.out.println(l[0].detalhes());
        System.out.println(l[1].detalhes());
        System.out.println(l[2].detalhes());
    }
}
