package aula11;

public class Aula11
{
    public static void main(String[] args)
    {
//        Pessoa p1 = new Pessoa();		// Classe abstrata
		
        
//        Visitante v1  = new Visitante();	// Heranca pobre (Heranca de implmentacao)
//        v1.setNome("Everton Hill");
//        v1.setIdade(5);
//        v1.fazerAniversaro();
//        v1.setSexo("M");
//        System.out.println(v1.toString());
        
        
//        Aluno a1 = new Aluno();			// Heranca pra diferenca
//        a1.setNome("Priscila Lima");
//        a1.setIdade(25);
//        a1.setMatricula(111);
//        a1.setCurso("ADS");
//        a1.setSexo("F");
//        a1.pagarMensalidade();
//        System.out.println(a1.toString());
        
        
        Bolsista b1 = new Bolsista();
        b1.setNome("Mick Hill");
        b1.setBolsa(50f);
        b1.setIdade(23);
        b1.setMatricula(222);
        b1.setCurso("CCOMP");
        b1.setSexo("M");
        b1.pagarMensalidade();
    }
}