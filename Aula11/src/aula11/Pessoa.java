package aula11;

public abstract class Pessoa
{
    // Atributos
    protected String nome;
    protected int idade;
    protected String sexo;
    
    
    
    // Metodos Publicos
    public void fazerAniversaro()
    {
        this.idade++;
    }
    
    
    
    // Metodos Especiais
    @Override
    public String toString() {
        return "Pessoa{" + "nome=" + nome + ", idade=" + idade + ", sexo=" + sexo + '}';
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }
}