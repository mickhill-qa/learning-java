package aula12;

public class Mamifero extends Animal
{
    // Atributos
    private String corDoPelo;
    
    
    // Metodos Publicos
    @Override
    public void locomover()
    {
        System.out.println("Correndo");
    }

    @Override
    public void alimentar()
    {
        System.out.println("Mamando");
    }

    @Override
    public void emitirSom()
    {
        System.out.println("Som de mamifero");
    }
    
    
    // Metodos Especiais
    public String getCorDoPelo() {
        return corDoPelo;
    }

    public void setCorDoPelo(String corDoPelo) {
        this.corDoPelo = corDoPelo;
    }
}