package aula12;

public class Peixe extends Animal
{
    // Atributos
    private String corEscama;
    
    
    // Metodos Publicos
    @Override
    public void locomover()
    {
        System.out.println("Nadando");
    }

    @Override
    public void alimentar()
    {
        System.out.println("Comendo Substancia");
    }

    @Override
    public void emitirSom()
    {
        System.out.println("Peixe nao faz Som");
    }
    
    public void soltarBolha()
    {
        System.out.println("Soltou uma bolhas");
    }
    
    
    // Metodos Especiais
    public String getCorEscama() {
        return corEscama;
    }

    public void setCorEscama(String corEscama) {
        this.corEscama = corEscama;
    }
}
