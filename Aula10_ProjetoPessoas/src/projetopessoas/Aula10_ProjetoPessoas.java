package projetopessoas;

public class Aula10_ProjetoPessoas
{
    public static void main(String[] args) 
    {
        Pessoa      p1 = new Pessoa();
        Aluno       p2 = new Aluno();
        Professor   p3 = new Professor();
        Funcionario p4 = new Funcionario();
        
        p1.setNome("Mick Hill");
        p2.setNome("Priscila");
        p3.setNome("Everton Hill");
        p4.setNome("Eliza Hill");
        
        p1.setSexo("M");
        p2.setSexo("F");
        p3.setSexo("M");
        p4.setSexo("F");

        p1.setIdade(23);
        p2.setIdade(25);
        p3.setIdade(1);
        
        p2.setCurso("CCOMP");
        p3.setSalario(2300f);
        p4.setSetor("Homologacao");
        
        p3.receberAumento(500f);

        System.out.println(p1.toString());
        System.out.println(p2.toString());
        System.out.println(p3.toString());
        System.out.println(p4.toString());
    }
}